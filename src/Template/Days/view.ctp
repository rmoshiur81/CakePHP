<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Day'), ['action' => 'edit', $day->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Day'), ['action' => 'delete', $day->id], ['confirm' => __('Are you sure you want to delete # {0}?', $day->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Days'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Day'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Day Slot'), ['controller' => 'DaySlot', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Day Slot'), ['controller' => 'DaySlot', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="days view large-9 medium-8 columns content">
    <h3><?= h($day->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($day->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($day->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($day->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($day->updated_at) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Day Slot') ?></h4>
        <?php if (!empty($day->day_slot)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Day Id') ?></th>
                <th scope="col"><?= __('Slot Id') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($day->day_slot as $daySlot): ?>
            <tr>
                <td><?= h($daySlot->day_id) ?></td>
                <td><?= h($daySlot->slot_id) ?></td>
                <td><?= h($daySlot->created_at) ?></td>
                <td><?= h($daySlot->updated_at) ?></td>
                <td><?= h($daySlot->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DaySlot', 'action' => 'view', $daySlot->day_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DaySlot', 'action' => 'edit', $daySlot->day_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DaySlot', 'action' => 'delete', $daySlot->day_id], ['confirm' => __('Are you sure you want to delete # {0}?', $daySlot->day_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
