<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Class In Holiday Permission'), ['action' => 'edit', $classInHolidayPermission->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Class In Holiday Permission'), ['action' => 'delete', $classInHolidayPermission->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classInHolidayPermission->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Class In Holiday Permissions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Class In Holiday Permission'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Holidays'), ['controller' => 'Holidays', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Holiday'), ['controller' => 'Holidays', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Booking Permissions'), ['controller' => 'BookingPermissions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Booking Permission'), ['controller' => 'BookingPermissions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Class Schedule Dates'), ['controller' => 'ClassScheduleDates', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Class Schedule Date'), ['controller' => 'ClassScheduleDates', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="classInHolidayPermissions view large-9 medium-8 columns content">
    <h3><?= h($classInHolidayPermission->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $classInHolidayPermission->has('user') ? $this->Html->link($classInHolidayPermission->user->name, ['controller' => 'Users', 'action' => 'view', $classInHolidayPermission->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Holiday') ?></th>
            <td><?= $classInHolidayPermission->has('holiday') ? $this->Html->link($classInHolidayPermission->holiday->id, ['controller' => 'Holidays', 'action' => 'view', $classInHolidayPermission->holiday->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Booking Permission') ?></th>
            <td><?= $classInHolidayPermission->has('booking_permission') ? $this->Html->link($classInHolidayPermission->booking_permission->id, ['controller' => 'BookingPermissions', 'action' => 'view', $classInHolidayPermission->booking_permission->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Comment') ?></th>
            <td><?= h($classInHolidayPermission->comment) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($classInHolidayPermission->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($classInHolidayPermission->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($classInHolidayPermission->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($classInHolidayPermission->deleted_at) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Approve Status') ?></h4>
        <?= $this->Text->autoParagraph(h($classInHolidayPermission->approve_status)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Class Schedule Dates') ?></h4>
        <?php if (!empty($classInHolidayPermission->class_schedule_dates)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Booking Permission Id') ?></th>
                <th scope="col"><?= __('Class In Holiday Permission Id') ?></th>
                <th scope="col"><?= __('Class No') ?></th>
                <th scope="col"><?= __('Class Date') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($classInHolidayPermission->class_schedule_dates as $classScheduleDates): ?>
            <tr>
                <td><?= h($classScheduleDates->id) ?></td>
                <td><?= h($classScheduleDates->booking_permission_id) ?></td>
                <td><?= h($classScheduleDates->class_in_holiday_permission_id) ?></td>
                <td><?= h($classScheduleDates->class_no) ?></td>
                <td><?= h($classScheduleDates->class_date) ?></td>
                <td><?= h($classScheduleDates->created_at) ?></td>
                <td><?= h($classScheduleDates->updated_at) ?></td>
                <td><?= h($classScheduleDates->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ClassScheduleDates', 'action' => 'view', $classScheduleDates->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ClassScheduleDates', 'action' => 'edit', $classScheduleDates->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ClassScheduleDates', 'action' => 'delete', $classScheduleDates->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classScheduleDates->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
