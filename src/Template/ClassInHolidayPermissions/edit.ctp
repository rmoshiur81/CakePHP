<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $classInHolidayPermission->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $classInHolidayPermission->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Class In Holiday Permissions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Holidays'), ['controller' => 'Holidays', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Holiday'), ['controller' => 'Holidays', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Booking Permissions'), ['controller' => 'BookingPermissions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Booking Permission'), ['controller' => 'BookingPermissions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Class Schedule Dates'), ['controller' => 'ClassScheduleDates', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Class Schedule Date'), ['controller' => 'ClassScheduleDates', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="classInHolidayPermissions form large-9 medium-8 columns content">
    <?= $this->Form->create($classInHolidayPermission) ?>
    <fieldset>
        <legend><?= __('Edit Class In Holiday Permission') ?></legend>
        <?php
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('holiday_id', ['options' => $holidays]);
            echo $this->Form->input('booking_permission_id', ['options' => $bookingPermissions]);
            echo $this->Form->input('comment');
            echo $this->Form->input('approve_status');
            echo $this->Form->input('created_at');
            echo $this->Form->input('updated_at');
            echo $this->Form->input('deleted_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
