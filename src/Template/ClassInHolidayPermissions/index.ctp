<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Class In Holiday Permission'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Holidays'), ['controller' => 'Holidays', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Holiday'), ['controller' => 'Holidays', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Booking Permissions'), ['controller' => 'BookingPermissions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Booking Permission'), ['controller' => 'BookingPermissions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Class Schedule Dates'), ['controller' => 'ClassScheduleDates', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Class Schedule Date'), ['controller' => 'ClassScheduleDates', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="classInHolidayPermissions index large-9 medium-8 columns content">
    <h3><?= __('Class In Holiday Permissions') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('holiday_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('booking_permission_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('comment') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('deleted_at') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($classInHolidayPermissions as $classInHolidayPermission): ?>
            <tr>
                <td><?= $this->Number->format($classInHolidayPermission->id) ?></td>
                <td><?= $classInHolidayPermission->has('user') ? $this->Html->link($classInHolidayPermission->user->name, ['controller' => 'Users', 'action' => 'view', $classInHolidayPermission->user->id]) : '' ?></td>
                <td><?= $classInHolidayPermission->has('holiday') ? $this->Html->link($classInHolidayPermission->holiday->id, ['controller' => 'Holidays', 'action' => 'view', $classInHolidayPermission->holiday->id]) : '' ?></td>
                <td><?= $classInHolidayPermission->has('booking_permission') ? $this->Html->link($classInHolidayPermission->booking_permission->id, ['controller' => 'BookingPermissions', 'action' => 'view', $classInHolidayPermission->booking_permission->id]) : '' ?></td>
                <td><?= h($classInHolidayPermission->comment) ?></td>
                <td><?= h($classInHolidayPermission->created_at) ?></td>
                <td><?= h($classInHolidayPermission->updated_at) ?></td>
                <td><?= h($classInHolidayPermission->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $classInHolidayPermission->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $classInHolidayPermission->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $classInHolidayPermission->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classInHolidayPermission->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
