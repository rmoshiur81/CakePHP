<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Role'), ['controller' => 'Roles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Booking Permissions'), ['controller' => 'BookingPermissions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Booking Permission'), ['controller' => 'BookingPermissions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Booking Requests'), ['controller' => 'BookingRequests', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Booking Request'), ['controller' => 'BookingRequests', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Class In Holiday Permissions'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Class In Holiday Permission'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Class In Holiday Requests'), ['controller' => 'ClassInHolidayRequests', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Class In Holiday Request'), ['controller' => 'ClassInHolidayRequests', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Profiles'), ['controller' => 'Profiles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Profile'), ['controller' => 'Profiles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Role') ?></th>
            <td><?= $user->has('role') ? $this->Html->link($user->role->name, ['controller' => 'Roles', 'action' => 'view', $user->role->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($user->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Remember Token') ?></th>
            <td><?= h($user->remember_token) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($user->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($user->updated_at) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Booking Permissions') ?></h4>
        <?php if (!empty($user->booking_permissions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Booking Request Id') ?></th>
                <th scope="col"><?= __('Batch Name') ?></th>
                <th scope="col"><?= __('Start Date') ?></th>
                <th scope="col"><?= __('End Date') ?></th>
                <th scope="col"><?= __('Duratiion') ?></th>
                <th scope="col"><?= __('Booking Status') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->booking_permissions as $bookingPermissions): ?>
            <tr>
                <td><?= h($bookingPermissions->id) ?></td>
                <td><?= h($bookingPermissions->user_id) ?></td>
                <td><?= h($bookingPermissions->booking_request_id) ?></td>
                <td><?= h($bookingPermissions->batch_name) ?></td>
                <td><?= h($bookingPermissions->start_date) ?></td>
                <td><?= h($bookingPermissions->end_date) ?></td>
                <td><?= h($bookingPermissions->duratiion) ?></td>
                <td><?= h($bookingPermissions->booking_status) ?></td>
                <td><?= h($bookingPermissions->created_at) ?></td>
                <td><?= h($bookingPermissions->updated_at) ?></td>
                <td><?= h($bookingPermissions->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'BookingPermissions', 'action' => 'view', $bookingPermissions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'BookingPermissions', 'action' => 'edit', $bookingPermissions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'BookingPermissions', 'action' => 'delete', $bookingPermissions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bookingPermissions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Booking Requests') ?></h4>
        <?php if (!empty($user->booking_requests)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Slot Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Lab Id') ?></th>
                <th scope="col"><?= __('Course Id') ?></th>
                <th scope="col"><?= __('Offered Course Name') ?></th>
                <th scope="col"><?= __('Expected Start Date') ?></th>
                <th scope="col"><?= __('Expected Duratiion') ?></th>
                <th scope="col"><?= __('Expected End Date') ?></th>
                <th scope="col"><?= __('Detail') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->booking_requests as $bookingRequests): ?>
            <tr>
                <td><?= h($bookingRequests->id) ?></td>
                <td><?= h($bookingRequests->slot_id) ?></td>
                <td><?= h($bookingRequests->user_id) ?></td>
                <td><?= h($bookingRequests->lab_id) ?></td>
                <td><?= h($bookingRequests->course_id) ?></td>
                <td><?= h($bookingRequests->offered_course_name) ?></td>
                <td><?= h($bookingRequests->expected_start_date) ?></td>
                <td><?= h($bookingRequests->expected_duratiion) ?></td>
                <td><?= h($bookingRequests->expected_end_date) ?></td>
                <td><?= h($bookingRequests->detail) ?></td>
                <td><?= h($bookingRequests->created_at) ?></td>
                <td><?= h($bookingRequests->updated_at) ?></td>
                <td><?= h($bookingRequests->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'BookingRequests', 'action' => 'view', $bookingRequests->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'BookingRequests', 'action' => 'edit', $bookingRequests->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'BookingRequests', 'action' => 'delete', $bookingRequests->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bookingRequests->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Class In Holiday Permissions') ?></h4>
        <?php if (!empty($user->class_in_holiday_permissions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Holiday Id') ?></th>
                <th scope="col"><?= __('Booking Permission Id') ?></th>
                <th scope="col"><?= __('Comment') ?></th>
                <th scope="col"><?= __('Approve Status') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->class_in_holiday_permissions as $classInHolidayPermissions): ?>
            <tr>
                <td><?= h($classInHolidayPermissions->id) ?></td>
                <td><?= h($classInHolidayPermissions->user_id) ?></td>
                <td><?= h($classInHolidayPermissions->holiday_id) ?></td>
                <td><?= h($classInHolidayPermissions->booking_permission_id) ?></td>
                <td><?= h($classInHolidayPermissions->comment) ?></td>
                <td><?= h($classInHolidayPermissions->approve_status) ?></td>
                <td><?= h($classInHolidayPermissions->created_at) ?></td>
                <td><?= h($classInHolidayPermissions->updated_at) ?></td>
                <td><?= h($classInHolidayPermissions->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'view', $classInHolidayPermissions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'edit', $classInHolidayPermissions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'delete', $classInHolidayPermissions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classInHolidayPermissions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Class In Holiday Requests') ?></h4>
        <?php if (!empty($user->class_in_holiday_requests)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Holiday Id') ?></th>
                <th scope="col"><?= __('Expected Date') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->class_in_holiday_requests as $classInHolidayRequests): ?>
            <tr>
                <td><?= h($classInHolidayRequests->id) ?></td>
                <td><?= h($classInHolidayRequests->user_id) ?></td>
                <td><?= h($classInHolidayRequests->holiday_id) ?></td>
                <td><?= h($classInHolidayRequests->expected_date) ?></td>
                <td><?= h($classInHolidayRequests->description) ?></td>
                <td><?= h($classInHolidayRequests->created_at) ?></td>
                <td><?= h($classInHolidayRequests->updated_at) ?></td>
                <td><?= h($classInHolidayRequests->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ClassInHolidayRequests', 'action' => 'view', $classInHolidayRequests->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ClassInHolidayRequests', 'action' => 'edit', $classInHolidayRequests->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ClassInHolidayRequests', 'action' => 'delete', $classInHolidayRequests->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classInHolidayRequests->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Profiles') ?></h4>
        <?php if (!empty($user->profiles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Profile Picture Name') ?></th>
                <th scope="col"><?= __('Profile Picture Url') ?></th>
                <th scope="col"><?= __('Permanent Address') ?></th>
                <th scope="col"><?= __('Present Address') ?></th>
                <th scope="col"><?= __('Contact No') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->profiles as $profiles): ?>
            <tr>
                <td><?= h($profiles->id) ?></td>
                <td><?= h($profiles->user_id) ?></td>
                <td><?= h($profiles->profile_picture_name) ?></td>
                <td><?= h($profiles->profile_picture_url) ?></td>
                <td><?= h($profiles->permanent_address) ?></td>
                <td><?= h($profiles->present_address) ?></td>
                <td><?= h($profiles->contact_no) ?></td>
                <td><?= h($profiles->created_at) ?></td>
                <td><?= h($profiles->updated_at) ?></td>
                <td><?= h($profiles->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Profiles', 'action' => 'view', $profiles->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Profiles', 'action' => 'edit', $profiles->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Profiles', 'action' => 'delete', $profiles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $profiles->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
