<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $slot->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $slot->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Slots'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Booking Requests'), ['controller' => 'BookingRequests', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Booking Request'), ['controller' => 'BookingRequests', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Day Slot'), ['controller' => 'DaySlot', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Day Slot'), ['controller' => 'DaySlot', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="slots form large-9 medium-8 columns content">
    <?= $this->Form->create($slot) ?>
    <fieldset>
        <legend><?= __('Edit Slot') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('start_time');
            echo $this->Form->input('end_time');
            echo $this->Form->input('duration');
            echo $this->Form->input('created_at');
            echo $this->Form->input('updated_at');
            echo $this->Form->input('deleted_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
