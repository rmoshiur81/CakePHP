<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Slot'), ['action' => 'edit', $slot->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Slot'), ['action' => 'delete', $slot->id], ['confirm' => __('Are you sure you want to delete # {0}?', $slot->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Slots'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Slot'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Booking Requests'), ['controller' => 'BookingRequests', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Booking Request'), ['controller' => 'BookingRequests', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Day Slot'), ['controller' => 'DaySlot', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Day Slot'), ['controller' => 'DaySlot', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="slots view large-9 medium-8 columns content">
    <h3><?= h($slot->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($slot->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($slot->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Duration') ?></th>
            <td><?= $this->Number->format($slot->duration) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Start Time') ?></th>
            <td><?= h($slot->start_time) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('End Time') ?></th>
            <td><?= h($slot->end_time) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($slot->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($slot->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($slot->deleted_at) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Booking Requests') ?></h4>
        <?php if (!empty($slot->booking_requests)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Slot Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Lab Id') ?></th>
                <th scope="col"><?= __('Course Id') ?></th>
                <th scope="col"><?= __('Offered Course Name') ?></th>
                <th scope="col"><?= __('Expected Start Date') ?></th>
                <th scope="col"><?= __('Expected Duratiion') ?></th>
                <th scope="col"><?= __('Expected End Date') ?></th>
                <th scope="col"><?= __('Detail') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($slot->booking_requests as $bookingRequests): ?>
            <tr>
                <td><?= h($bookingRequests->id) ?></td>
                <td><?= h($bookingRequests->slot_id) ?></td>
                <td><?= h($bookingRequests->user_id) ?></td>
                <td><?= h($bookingRequests->lab_id) ?></td>
                <td><?= h($bookingRequests->course_id) ?></td>
                <td><?= h($bookingRequests->offered_course_name) ?></td>
                <td><?= h($bookingRequests->expected_start_date) ?></td>
                <td><?= h($bookingRequests->expected_duratiion) ?></td>
                <td><?= h($bookingRequests->expected_end_date) ?></td>
                <td><?= h($bookingRequests->detail) ?></td>
                <td><?= h($bookingRequests->created_at) ?></td>
                <td><?= h($bookingRequests->updated_at) ?></td>
                <td><?= h($bookingRequests->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'BookingRequests', 'action' => 'view', $bookingRequests->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'BookingRequests', 'action' => 'edit', $bookingRequests->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'BookingRequests', 'action' => 'delete', $bookingRequests->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bookingRequests->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Day Slot') ?></h4>
        <?php if (!empty($slot->day_slot)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Day Id') ?></th>
                <th scope="col"><?= __('Slot Id') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($slot->day_slot as $daySlot): ?>
            <tr>
                <td><?= h($daySlot->day_id) ?></td>
                <td><?= h($daySlot->slot_id) ?></td>
                <td><?= h($daySlot->created_at) ?></td>
                <td><?= h($daySlot->updated_at) ?></td>
                <td><?= h($daySlot->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DaySlot', 'action' => 'view', $daySlot->day_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DaySlot', 'action' => 'edit', $daySlot->day_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DaySlot', 'action' => 'delete', $daySlot->day_id], ['confirm' => __('Are you sure you want to delete # {0}?', $daySlot->day_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
