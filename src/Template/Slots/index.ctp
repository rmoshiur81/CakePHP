<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Slot'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Booking Requests'), ['controller' => 'BookingRequests', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Booking Request'), ['controller' => 'BookingRequests', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Day Slot'), ['controller' => 'DaySlot', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Day Slot'), ['controller' => 'DaySlot', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="slots index large-9 medium-8 columns content">
    <h3><?= __('Slots') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('start_time') ?></th>
                <th scope="col"><?= $this->Paginator->sort('end_time') ?></th>
                <th scope="col"><?= $this->Paginator->sort('duration') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('deleted_at') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($slots as $slot): ?>
            <tr>
                <td><?= $this->Number->format($slot->id) ?></td>
                <td><?= h($slot->name) ?></td>
                <td><?= h($slot->start_time) ?></td>
                <td><?= h($slot->end_time) ?></td>
                <td><?= $this->Number->format($slot->duration) ?></td>
                <td><?= h($slot->created_at) ?></td>
                <td><?= h($slot->updated_at) ?></td>
                <td><?= h($slot->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $slot->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $slot->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $slot->id], ['confirm' => __('Are you sure you want to delete # {0}?', $slot->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
