<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Labs'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Branches'), ['controller' => 'Branches', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Branch'), ['controller' => 'Branches', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Booking Requests'), ['controller' => 'BookingRequests', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Booking Request'), ['controller' => 'BookingRequests', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Course Lab'), ['controller' => 'CourseLab', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Course Lab'), ['controller' => 'CourseLab', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="labs form large-9 medium-8 columns content">
    <?= $this->Form->create($lab) ?>
    <fieldset>
        <legend><?= __('Add Lab') ?></legend>
        <?php
            echo $this->Form->input('branch_id', ['options' => $branches]);
            echo $this->Form->input('name');
            echo $this->Form->input('code');
            echo $this->Form->input('total_seat');
            echo $this->Form->input('created_at');
            echo $this->Form->input('updated_at');
            echo $this->Form->input('deleted_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
