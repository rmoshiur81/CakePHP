<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Lab'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Branches'), ['controller' => 'Branches', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Branch'), ['controller' => 'Branches', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Booking Requests'), ['controller' => 'BookingRequests', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Booking Request'), ['controller' => 'BookingRequests', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Course Lab'), ['controller' => 'CourseLab', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Course Lab'), ['controller' => 'CourseLab', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="labs index large-9 medium-8 columns content">
    <h3><?= __('Labs') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('branch_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('total_seat') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('deleted_at') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($labs as $lab): ?>
            <tr>
                <td><?= $this->Number->format($lab->id) ?></td>
                <td><?= $lab->has('branch') ? $this->Html->link($lab->branch->name, ['controller' => 'Branches', 'action' => 'view', $lab->branch->id]) : '' ?></td>
                <td><?= h($lab->name) ?></td>
                <td><?= h($lab->code) ?></td>
                <td><?= $this->Number->format($lab->total_seat) ?></td>
                <td><?= h($lab->created_at) ?></td>
                <td><?= h($lab->updated_at) ?></td>
                <td><?= h($lab->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $lab->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $lab->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $lab->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lab->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
