<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Day Slot'), ['action' => 'edit', $daySlot->day_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Day Slot'), ['action' => 'delete', $daySlot->day_id], ['confirm' => __('Are you sure you want to delete # {0}?', $daySlot->day_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Day Slot'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Day Slot'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Days'), ['controller' => 'Days', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Day'), ['controller' => 'Days', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Slots'), ['controller' => 'Slots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Slot'), ['controller' => 'Slots', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="daySlot view large-9 medium-8 columns content">
    <h3><?= h($daySlot->day_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Day') ?></th>
            <td><?= $daySlot->has('day') ? $this->Html->link($daySlot->day->name, ['controller' => 'Days', 'action' => 'view', $daySlot->day->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Slot') ?></th>
            <td><?= $daySlot->has('slot') ? $this->Html->link($daySlot->slot->name, ['controller' => 'Slots', 'action' => 'view', $daySlot->slot->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($daySlot->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($daySlot->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($daySlot->deleted_at) ?></td>
        </tr>
    </table>
</div>
