<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Day Slot'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Days'), ['controller' => 'Days', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Day'), ['controller' => 'Days', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Slots'), ['controller' => 'Slots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Slot'), ['controller' => 'Slots', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="daySlot index large-9 medium-8 columns content">
    <h3><?= __('Day Slot') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('day_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('slot_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('deleted_at') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($daySlot as $daySlot): ?>
            <tr>
                <td><?= $daySlot->has('day') ? $this->Html->link($daySlot->day->name, ['controller' => 'Days', 'action' => 'view', $daySlot->day->id]) : '' ?></td>
                <td><?= $daySlot->has('slot') ? $this->Html->link($daySlot->slot->name, ['controller' => 'Slots', 'action' => 'view', $daySlot->slot->id]) : '' ?></td>
                <td><?= h($daySlot->created_at) ?></td>
                <td><?= h($daySlot->updated_at) ?></td>
                <td><?= h($daySlot->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $daySlot->day_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $daySlot->day_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $daySlot->day_id], ['confirm' => __('Are you sure you want to delete # {0}?', $daySlot->day_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
