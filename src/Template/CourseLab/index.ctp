<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Course Lab'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Courses'), ['controller' => 'Courses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Course'), ['controller' => 'Courses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Labs'), ['controller' => 'Labs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Lab'), ['controller' => 'Labs', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="courseLab index large-9 medium-8 columns content">
    <h3><?= __('Course Lab') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('course_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lab_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('deleted_at') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($courseLab as $courseLab): ?>
            <tr>
                <td><?= $courseLab->has('course') ? $this->Html->link($courseLab->course->name, ['controller' => 'Courses', 'action' => 'view', $courseLab->course->id]) : '' ?></td>
                <td><?= $courseLab->has('lab') ? $this->Html->link($courseLab->lab->name, ['controller' => 'Labs', 'action' => 'view', $courseLab->lab->id]) : '' ?></td>
                <td><?= h($courseLab->created_at) ?></td>
                <td><?= h($courseLab->updated_at) ?></td>
                <td><?= h($courseLab->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $courseLab->course_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $courseLab->course_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $courseLab->course_id], ['confirm' => __('Are you sure you want to delete # {0}?', $courseLab->course_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
