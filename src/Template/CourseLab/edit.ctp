<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $courseLab->course_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $courseLab->course_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Course Lab'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Courses'), ['controller' => 'Courses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Course'), ['controller' => 'Courses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Labs'), ['controller' => 'Labs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Lab'), ['controller' => 'Labs', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="courseLab form large-9 medium-8 columns content">
    <?= $this->Form->create($courseLab) ?>
    <fieldset>
        <legend><?= __('Edit Course Lab') ?></legend>
        <?php
            echo $this->Form->input('created_at');
            echo $this->Form->input('updated_at');
            echo $this->Form->input('deleted_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
