<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Course Lab'), ['action' => 'edit', $courseLab->course_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Course Lab'), ['action' => 'delete', $courseLab->course_id], ['confirm' => __('Are you sure you want to delete # {0}?', $courseLab->course_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Course Lab'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Course Lab'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Courses'), ['controller' => 'Courses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Course'), ['controller' => 'Courses', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Labs'), ['controller' => 'Labs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Lab'), ['controller' => 'Labs', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="courseLab view large-9 medium-8 columns content">
    <h3><?= h($courseLab->course_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Course') ?></th>
            <td><?= $courseLab->has('course') ? $this->Html->link($courseLab->course->name, ['controller' => 'Courses', 'action' => 'view', $courseLab->course->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lab') ?></th>
            <td><?= $courseLab->has('lab') ? $this->Html->link($courseLab->lab->name, ['controller' => 'Labs', 'action' => 'view', $courseLab->lab->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($courseLab->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($courseLab->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($courseLab->deleted_at) ?></td>
        </tr>
    </table>
</div>
