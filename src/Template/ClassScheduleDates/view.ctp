<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Class Schedule Date'), ['action' => 'edit', $classScheduleDate->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Class Schedule Date'), ['action' => 'delete', $classScheduleDate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classScheduleDate->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Class Schedule Dates'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Class Schedule Date'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Booking Permissions'), ['controller' => 'BookingPermissions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Booking Permission'), ['controller' => 'BookingPermissions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Class In Holiday Permissions'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Class In Holiday Permission'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="classScheduleDates view large-9 medium-8 columns content">
    <h3><?= h($classScheduleDate->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Booking Permission') ?></th>
            <td><?= $classScheduleDate->has('booking_permission') ? $this->Html->link($classScheduleDate->booking_permission->id, ['controller' => 'BookingPermissions', 'action' => 'view', $classScheduleDate->booking_permission->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class In Holiday Permission') ?></th>
            <td><?= $classScheduleDate->has('class_in_holiday_permission') ? $this->Html->link($classScheduleDate->class_in_holiday_permission->id, ['controller' => 'ClassInHolidayPermissions', 'action' => 'view', $classScheduleDate->class_in_holiday_permission->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($classScheduleDate->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class No') ?></th>
            <td><?= $this->Number->format($classScheduleDate->class_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class Date') ?></th>
            <td><?= h($classScheduleDate->class_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($classScheduleDate->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($classScheduleDate->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($classScheduleDate->deleted_at) ?></td>
        </tr>
    </table>
</div>
