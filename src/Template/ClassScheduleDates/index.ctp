<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Class Schedule Date'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Booking Permissions'), ['controller' => 'BookingPermissions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Booking Permission'), ['controller' => 'BookingPermissions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Class In Holiday Permissions'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Class In Holiday Permission'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="classScheduleDates index large-9 medium-8 columns content">
    <h3><?= __('Class Schedule Dates') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('booking_permission_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('class_in_holiday_permission_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('class_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('class_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('deleted_at') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($classScheduleDates as $classScheduleDate): ?>
            <tr>
                <td><?= $this->Number->format($classScheduleDate->id) ?></td>
                <td><?= $classScheduleDate->has('booking_permission') ? $this->Html->link($classScheduleDate->booking_permission->id, ['controller' => 'BookingPermissions', 'action' => 'view', $classScheduleDate->booking_permission->id]) : '' ?></td>
                <td><?= $classScheduleDate->has('class_in_holiday_permission') ? $this->Html->link($classScheduleDate->class_in_holiday_permission->id, ['controller' => 'ClassInHolidayPermissions', 'action' => 'view', $classScheduleDate->class_in_holiday_permission->id]) : '' ?></td>
                <td><?= $this->Number->format($classScheduleDate->class_no) ?></td>
                <td><?= h($classScheduleDate->class_date) ?></td>
                <td><?= h($classScheduleDate->created_at) ?></td>
                <td><?= h($classScheduleDate->updated_at) ?></td>
                <td><?= h($classScheduleDate->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $classScheduleDate->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $classScheduleDate->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $classScheduleDate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classScheduleDate->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
