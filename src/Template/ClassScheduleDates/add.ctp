<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Class Schedule Dates'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Booking Permissions'), ['controller' => 'BookingPermissions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Booking Permission'), ['controller' => 'BookingPermissions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Class In Holiday Permissions'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Class In Holiday Permission'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="classScheduleDates form large-9 medium-8 columns content">
    <?= $this->Form->create($classScheduleDate) ?>
    <fieldset>
        <legend><?= __('Add Class Schedule Date') ?></legend>
        <?php
            echo $this->Form->input('booking_permission_id', ['options' => $bookingPermissions]);
            echo $this->Form->input('class_in_holiday_permission_id', ['options' => $classInHolidayPermissions]);
            echo $this->Form->input('class_no');
            echo $this->Form->input('class_date');
            echo $this->Form->input('created_at');
            echo $this->Form->input('updated_at');
            echo $this->Form->input('deleted_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
