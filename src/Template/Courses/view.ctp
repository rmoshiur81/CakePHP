<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Course'), ['action' => 'edit', $course->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Course'), ['action' => 'delete', $course->id], ['confirm' => __('Are you sure you want to delete # {0}?', $course->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Courses'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Course'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Booking Requests'), ['controller' => 'BookingRequests', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Booking Request'), ['controller' => 'BookingRequests', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Course Lab'), ['controller' => 'CourseLab', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Course Lab'), ['controller' => 'CourseLab', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="courses view large-9 medium-8 columns content">
    <h3><?= h($course->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($course->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Code') ?></th>
            <td><?= h($course->code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($course->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($course->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($course->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($course->deleted_at) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Booking Requests') ?></h4>
        <?php if (!empty($course->booking_requests)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Slot Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Lab Id') ?></th>
                <th scope="col"><?= __('Course Id') ?></th>
                <th scope="col"><?= __('Offered Course Name') ?></th>
                <th scope="col"><?= __('Expected Start Date') ?></th>
                <th scope="col"><?= __('Expected Duratiion') ?></th>
                <th scope="col"><?= __('Expected End Date') ?></th>
                <th scope="col"><?= __('Detail') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($course->booking_requests as $bookingRequests): ?>
            <tr>
                <td><?= h($bookingRequests->id) ?></td>
                <td><?= h($bookingRequests->slot_id) ?></td>
                <td><?= h($bookingRequests->user_id) ?></td>
                <td><?= h($bookingRequests->lab_id) ?></td>
                <td><?= h($bookingRequests->course_id) ?></td>
                <td><?= h($bookingRequests->offered_course_name) ?></td>
                <td><?= h($bookingRequests->expected_start_date) ?></td>
                <td><?= h($bookingRequests->expected_duratiion) ?></td>
                <td><?= h($bookingRequests->expected_end_date) ?></td>
                <td><?= h($bookingRequests->detail) ?></td>
                <td><?= h($bookingRequests->created_at) ?></td>
                <td><?= h($bookingRequests->updated_at) ?></td>
                <td><?= h($bookingRequests->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'BookingRequests', 'action' => 'view', $bookingRequests->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'BookingRequests', 'action' => 'edit', $bookingRequests->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'BookingRequests', 'action' => 'delete', $bookingRequests->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bookingRequests->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Course Lab') ?></h4>
        <?php if (!empty($course->course_lab)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Course Id') ?></th>
                <th scope="col"><?= __('Lab Id') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($course->course_lab as $courseLab): ?>
            <tr>
                <td><?= h($courseLab->course_id) ?></td>
                <td><?= h($courseLab->lab_id) ?></td>
                <td><?= h($courseLab->created_at) ?></td>
                <td><?= h($courseLab->updated_at) ?></td>
                <td><?= h($courseLab->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CourseLab', 'action' => 'view', $courseLab->course_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CourseLab', 'action' => 'edit', $courseLab->course_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CourseLab', 'action' => 'delete', $courseLab->course_id], ['confirm' => __('Are you sure you want to delete # {0}?', $courseLab->course_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
