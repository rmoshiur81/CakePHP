<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $bookingPermission->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $bookingPermission->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Booking Permissions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Booking Requests'), ['controller' => 'BookingRequests', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Booking Request'), ['controller' => 'BookingRequests', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Class In Holiday Permissions'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Class In Holiday Permission'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Class Schedule Dates'), ['controller' => 'ClassScheduleDates', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Class Schedule Date'), ['controller' => 'ClassScheduleDates', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="bookingPermissions form large-9 medium-8 columns content">
    <?= $this->Form->create($bookingPermission) ?>
    <fieldset>
        <legend><?= __('Edit Booking Permission') ?></legend>
        <?php
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('booking_request_id', ['options' => $bookingRequests]);
            echo $this->Form->input('batch_name');
            echo $this->Form->input('start_date');
            echo $this->Form->input('end_date');
            echo $this->Form->input('duratiion');
            echo $this->Form->input('booking_status');
            echo $this->Form->input('created_at');
            echo $this->Form->input('updated_at');
            echo $this->Form->input('deleted_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
