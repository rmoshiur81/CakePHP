<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Booking Permission'), ['action' => 'edit', $bookingPermission->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Booking Permission'), ['action' => 'delete', $bookingPermission->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bookingPermission->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Booking Permissions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Booking Permission'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Booking Requests'), ['controller' => 'BookingRequests', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Booking Request'), ['controller' => 'BookingRequests', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Class In Holiday Permissions'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Class In Holiday Permission'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Class Schedule Dates'), ['controller' => 'ClassScheduleDates', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Class Schedule Date'), ['controller' => 'ClassScheduleDates', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="bookingPermissions view large-9 medium-8 columns content">
    <h3><?= h($bookingPermission->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $bookingPermission->has('user') ? $this->Html->link($bookingPermission->user->name, ['controller' => 'Users', 'action' => 'view', $bookingPermission->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Booking Request') ?></th>
            <td><?= $bookingPermission->has('booking_request') ? $this->Html->link($bookingPermission->booking_request->id, ['controller' => 'BookingRequests', 'action' => 'view', $bookingPermission->booking_request->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Batch Name') ?></th>
            <td><?= h($bookingPermission->batch_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Duratiion') ?></th>
            <td><?= h($bookingPermission->duratiion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($bookingPermission->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Start Date') ?></th>
            <td><?= h($bookingPermission->start_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('End Date') ?></th>
            <td><?= h($bookingPermission->end_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($bookingPermission->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($bookingPermission->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($bookingPermission->deleted_at) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Booking Status') ?></h4>
        <?= $this->Text->autoParagraph(h($bookingPermission->booking_status)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Class In Holiday Permissions') ?></h4>
        <?php if (!empty($bookingPermission->class_in_holiday_permissions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Holiday Id') ?></th>
                <th scope="col"><?= __('Booking Permission Id') ?></th>
                <th scope="col"><?= __('Comment') ?></th>
                <th scope="col"><?= __('Approve Status') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($bookingPermission->class_in_holiday_permissions as $classInHolidayPermissions): ?>
            <tr>
                <td><?= h($classInHolidayPermissions->id) ?></td>
                <td><?= h($classInHolidayPermissions->user_id) ?></td>
                <td><?= h($classInHolidayPermissions->holiday_id) ?></td>
                <td><?= h($classInHolidayPermissions->booking_permission_id) ?></td>
                <td><?= h($classInHolidayPermissions->comment) ?></td>
                <td><?= h($classInHolidayPermissions->approve_status) ?></td>
                <td><?= h($classInHolidayPermissions->created_at) ?></td>
                <td><?= h($classInHolidayPermissions->updated_at) ?></td>
                <td><?= h($classInHolidayPermissions->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'view', $classInHolidayPermissions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'edit', $classInHolidayPermissions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'delete', $classInHolidayPermissions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classInHolidayPermissions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Class Schedule Dates') ?></h4>
        <?php if (!empty($bookingPermission->class_schedule_dates)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Booking Permission Id') ?></th>
                <th scope="col"><?= __('Class In Holiday Permission Id') ?></th>
                <th scope="col"><?= __('Class No') ?></th>
                <th scope="col"><?= __('Class Date') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($bookingPermission->class_schedule_dates as $classScheduleDates): ?>
            <tr>
                <td><?= h($classScheduleDates->id) ?></td>
                <td><?= h($classScheduleDates->booking_permission_id) ?></td>
                <td><?= h($classScheduleDates->class_in_holiday_permission_id) ?></td>
                <td><?= h($classScheduleDates->class_no) ?></td>
                <td><?= h($classScheduleDates->class_date) ?></td>
                <td><?= h($classScheduleDates->created_at) ?></td>
                <td><?= h($classScheduleDates->updated_at) ?></td>
                <td><?= h($classScheduleDates->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ClassScheduleDates', 'action' => 'view', $classScheduleDates->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ClassScheduleDates', 'action' => 'edit', $classScheduleDates->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ClassScheduleDates', 'action' => 'delete', $classScheduleDates->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classScheduleDates->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
