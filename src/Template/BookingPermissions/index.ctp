<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Booking Permission'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Booking Requests'), ['controller' => 'BookingRequests', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Booking Request'), ['controller' => 'BookingRequests', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Class In Holiday Permissions'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Class In Holiday Permission'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Class Schedule Dates'), ['controller' => 'ClassScheduleDates', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Class Schedule Date'), ['controller' => 'ClassScheduleDates', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="bookingPermissions index large-9 medium-8 columns content">
    <h3><?= __('Booking Permissions') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('booking_request_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('batch_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('start_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('end_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('duratiion') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('deleted_at') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($bookingPermissions as $bookingPermission): ?>
            <tr>
                <td><?= $this->Number->format($bookingPermission->id) ?></td>
                <td><?= $bookingPermission->has('user') ? $this->Html->link($bookingPermission->user->name, ['controller' => 'Users', 'action' => 'view', $bookingPermission->user->id]) : '' ?></td>
                <td><?= $bookingPermission->has('booking_request') ? $this->Html->link($bookingPermission->booking_request->id, ['controller' => 'BookingRequests', 'action' => 'view', $bookingPermission->booking_request->id]) : '' ?></td>
                <td><?= h($bookingPermission->batch_name) ?></td>
                <td><?= h($bookingPermission->start_date) ?></td>
                <td><?= h($bookingPermission->end_date) ?></td>
                <td><?= h($bookingPermission->duratiion) ?></td>
                <td><?= h($bookingPermission->created_at) ?></td>
                <td><?= h($bookingPermission->updated_at) ?></td>
                <td><?= h($bookingPermission->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $bookingPermission->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $bookingPermission->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bookingPermission->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bookingPermission->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
