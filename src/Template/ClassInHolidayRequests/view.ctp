<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Class In Holiday Request'), ['action' => 'edit', $classInHolidayRequest->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Class In Holiday Request'), ['action' => 'delete', $classInHolidayRequest->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classInHolidayRequest->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Class In Holiday Requests'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Class In Holiday Request'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Holidays'), ['controller' => 'Holidays', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Holiday'), ['controller' => 'Holidays', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="classInHolidayRequests view large-9 medium-8 columns content">
    <h3><?= h($classInHolidayRequest->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $classInHolidayRequest->has('user') ? $this->Html->link($classInHolidayRequest->user->name, ['controller' => 'Users', 'action' => 'view', $classInHolidayRequest->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Holiday') ?></th>
            <td><?= $classInHolidayRequest->has('holiday') ? $this->Html->link($classInHolidayRequest->holiday->id, ['controller' => 'Holidays', 'action' => 'view', $classInHolidayRequest->holiday->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($classInHolidayRequest->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Expected Date') ?></th>
            <td><?= h($classInHolidayRequest->expected_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($classInHolidayRequest->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($classInHolidayRequest->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($classInHolidayRequest->deleted_at) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($classInHolidayRequest->description)); ?>
    </div>
</div>
