<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Class In Holiday Request'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Holidays'), ['controller' => 'Holidays', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Holiday'), ['controller' => 'Holidays', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="classInHolidayRequests index large-9 medium-8 columns content">
    <h3><?= __('Class In Holiday Requests') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('holiday_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('expected_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('deleted_at') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($classInHolidayRequests as $classInHolidayRequest): ?>
            <tr>
                <td><?= $this->Number->format($classInHolidayRequest->id) ?></td>
                <td><?= $classInHolidayRequest->has('user') ? $this->Html->link($classInHolidayRequest->user->name, ['controller' => 'Users', 'action' => 'view', $classInHolidayRequest->user->id]) : '' ?></td>
                <td><?= $classInHolidayRequest->has('holiday') ? $this->Html->link($classInHolidayRequest->holiday->id, ['controller' => 'Holidays', 'action' => 'view', $classInHolidayRequest->holiday->id]) : '' ?></td>
                <td><?= h($classInHolidayRequest->expected_date) ?></td>
                <td><?= h($classInHolidayRequest->created_at) ?></td>
                <td><?= h($classInHolidayRequest->updated_at) ?></td>
                <td><?= h($classInHolidayRequest->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $classInHolidayRequest->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $classInHolidayRequest->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $classInHolidayRequest->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classInHolidayRequest->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
