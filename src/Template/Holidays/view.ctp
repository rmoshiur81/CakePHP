<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Holiday'), ['action' => 'edit', $holiday->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Holiday'), ['action' => 'delete', $holiday->id], ['confirm' => __('Are you sure you want to delete # {0}?', $holiday->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Holidays'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Holiday'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Class In Holiday Permissions'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Class In Holiday Permission'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Class In Holiday Requests'), ['controller' => 'ClassInHolidayRequests', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Class In Holiday Request'), ['controller' => 'ClassInHolidayRequests', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="holidays view large-9 medium-8 columns content">
    <h3><?= h($holiday->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($holiday->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total Holiday') ?></th>
            <td><?= $this->Number->format($holiday->total_holiday) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Start Date') ?></th>
            <td><?= h($holiday->start_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('End Date') ?></th>
            <td><?= h($holiday->end_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($holiday->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($holiday->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($holiday->deleted_at) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Class In Holiday Permissions') ?></h4>
        <?php if (!empty($holiday->class_in_holiday_permissions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Holiday Id') ?></th>
                <th scope="col"><?= __('Booking Permission Id') ?></th>
                <th scope="col"><?= __('Comment') ?></th>
                <th scope="col"><?= __('Approve Status') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($holiday->class_in_holiday_permissions as $classInHolidayPermissions): ?>
            <tr>
                <td><?= h($classInHolidayPermissions->id) ?></td>
                <td><?= h($classInHolidayPermissions->user_id) ?></td>
                <td><?= h($classInHolidayPermissions->holiday_id) ?></td>
                <td><?= h($classInHolidayPermissions->booking_permission_id) ?></td>
                <td><?= h($classInHolidayPermissions->comment) ?></td>
                <td><?= h($classInHolidayPermissions->approve_status) ?></td>
                <td><?= h($classInHolidayPermissions->created_at) ?></td>
                <td><?= h($classInHolidayPermissions->updated_at) ?></td>
                <td><?= h($classInHolidayPermissions->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'view', $classInHolidayPermissions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'edit', $classInHolidayPermissions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'delete', $classInHolidayPermissions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classInHolidayPermissions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Class In Holiday Requests') ?></h4>
        <?php if (!empty($holiday->class_in_holiday_requests)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Holiday Id') ?></th>
                <th scope="col"><?= __('Expected Date') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($holiday->class_in_holiday_requests as $classInHolidayRequests): ?>
            <tr>
                <td><?= h($classInHolidayRequests->id) ?></td>
                <td><?= h($classInHolidayRequests->user_id) ?></td>
                <td><?= h($classInHolidayRequests->holiday_id) ?></td>
                <td><?= h($classInHolidayRequests->expected_date) ?></td>
                <td><?= h($classInHolidayRequests->description) ?></td>
                <td><?= h($classInHolidayRequests->created_at) ?></td>
                <td><?= h($classInHolidayRequests->updated_at) ?></td>
                <td><?= h($classInHolidayRequests->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ClassInHolidayRequests', 'action' => 'view', $classInHolidayRequests->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ClassInHolidayRequests', 'action' => 'edit', $classInHolidayRequests->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ClassInHolidayRequests', 'action' => 'delete', $classInHolidayRequests->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classInHolidayRequests->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
