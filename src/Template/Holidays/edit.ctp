<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $holiday->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $holiday->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Holidays'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Class In Holiday Permissions'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Class In Holiday Permission'), ['controller' => 'ClassInHolidayPermissions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Class In Holiday Requests'), ['controller' => 'ClassInHolidayRequests', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Class In Holiday Request'), ['controller' => 'ClassInHolidayRequests', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="holidays form large-9 medium-8 columns content">
    <?= $this->Form->create($holiday) ?>
    <fieldset>
        <legend><?= __('Edit Holiday') ?></legend>
        <?php
            echo $this->Form->input('start_date');
            echo $this->Form->input('total_holiday');
            echo $this->Form->input('end_date');
            echo $this->Form->input('created_at');
            echo $this->Form->input('updated_at');
            echo $this->Form->input('deleted_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
