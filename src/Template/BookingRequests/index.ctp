<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Booking Request'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Slots'), ['controller' => 'Slots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Slot'), ['controller' => 'Slots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Labs'), ['controller' => 'Labs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Lab'), ['controller' => 'Labs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Courses'), ['controller' => 'Courses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Course'), ['controller' => 'Courses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Booking Permissions'), ['controller' => 'BookingPermissions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Booking Permission'), ['controller' => 'BookingPermissions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="bookingRequests index large-9 medium-8 columns content">
    <h3><?= __('Booking Requests') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('slot_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lab_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('course_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('offered_course_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('expected_start_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('expected_duratiion') ?></th>
                <th scope="col"><?= $this->Paginator->sort('expected_end_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('deleted_at') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($bookingRequests as $bookingRequest): ?>
            <tr>
                <td><?= $this->Number->format($bookingRequest->id) ?></td>
                <td><?= $bookingRequest->has('slot') ? $this->Html->link($bookingRequest->slot->name, ['controller' => 'Slots', 'action' => 'view', $bookingRequest->slot->id]) : '' ?></td>
                <td><?= $bookingRequest->has('user') ? $this->Html->link($bookingRequest->user->name, ['controller' => 'Users', 'action' => 'view', $bookingRequest->user->id]) : '' ?></td>
                <td><?= $bookingRequest->has('lab') ? $this->Html->link($bookingRequest->lab->name, ['controller' => 'Labs', 'action' => 'view', $bookingRequest->lab->id]) : '' ?></td>
                <td><?= $bookingRequest->has('course') ? $this->Html->link($bookingRequest->course->name, ['controller' => 'Courses', 'action' => 'view', $bookingRequest->course->id]) : '' ?></td>
                <td><?= h($bookingRequest->offered_course_name) ?></td>
                <td><?= h($bookingRequest->expected_start_date) ?></td>
                <td><?= h($bookingRequest->expected_duratiion) ?></td>
                <td><?= h($bookingRequest->expected_end_date) ?></td>
                <td><?= h($bookingRequest->created_at) ?></td>
                <td><?= h($bookingRequest->updated_at) ?></td>
                <td><?= h($bookingRequest->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $bookingRequest->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $bookingRequest->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bookingRequest->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bookingRequest->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
