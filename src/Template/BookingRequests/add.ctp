<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Booking Requests'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Slots'), ['controller' => 'Slots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Slot'), ['controller' => 'Slots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Labs'), ['controller' => 'Labs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Lab'), ['controller' => 'Labs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Courses'), ['controller' => 'Courses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Course'), ['controller' => 'Courses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Booking Permissions'), ['controller' => 'BookingPermissions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Booking Permission'), ['controller' => 'BookingPermissions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="bookingRequests form large-9 medium-8 columns content">
    <?= $this->Form->create($bookingRequest) ?>
    <fieldset>
        <legend><?= __('Add Booking Request') ?></legend>
        <?php
            echo $this->Form->input('slot_id', ['options' => $slots]);
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('lab_id', ['options' => $labs]);
            echo $this->Form->input('course_id', ['options' => $courses]);
            echo $this->Form->input('offered_course_name');
            echo $this->Form->input('expected_start_date');
            echo $this->Form->input('expected_duratiion');
            echo $this->Form->input('expected_end_date');
            echo $this->Form->input('detail');
            echo $this->Form->input('created_at');
            echo $this->Form->input('updated_at');
            echo $this->Form->input('deleted_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
