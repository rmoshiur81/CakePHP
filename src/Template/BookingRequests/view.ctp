<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Booking Request'), ['action' => 'edit', $bookingRequest->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Booking Request'), ['action' => 'delete', $bookingRequest->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bookingRequest->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Booking Requests'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Booking Request'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Slots'), ['controller' => 'Slots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Slot'), ['controller' => 'Slots', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Labs'), ['controller' => 'Labs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Lab'), ['controller' => 'Labs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Courses'), ['controller' => 'Courses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Course'), ['controller' => 'Courses', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Booking Permissions'), ['controller' => 'BookingPermissions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Booking Permission'), ['controller' => 'BookingPermissions', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="bookingRequests view large-9 medium-8 columns content">
    <h3><?= h($bookingRequest->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Slot') ?></th>
            <td><?= $bookingRequest->has('slot') ? $this->Html->link($bookingRequest->slot->name, ['controller' => 'Slots', 'action' => 'view', $bookingRequest->slot->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $bookingRequest->has('user') ? $this->Html->link($bookingRequest->user->name, ['controller' => 'Users', 'action' => 'view', $bookingRequest->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lab') ?></th>
            <td><?= $bookingRequest->has('lab') ? $this->Html->link($bookingRequest->lab->name, ['controller' => 'Labs', 'action' => 'view', $bookingRequest->lab->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Course') ?></th>
            <td><?= $bookingRequest->has('course') ? $this->Html->link($bookingRequest->course->name, ['controller' => 'Courses', 'action' => 'view', $bookingRequest->course->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Offered Course Name') ?></th>
            <td><?= h($bookingRequest->offered_course_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Expected Duratiion') ?></th>
            <td><?= h($bookingRequest->expected_duratiion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($bookingRequest->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Expected Start Date') ?></th>
            <td><?= h($bookingRequest->expected_start_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Expected End Date') ?></th>
            <td><?= h($bookingRequest->expected_end_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($bookingRequest->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($bookingRequest->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($bookingRequest->deleted_at) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Detail') ?></h4>
        <?= $this->Text->autoParagraph(h($bookingRequest->detail)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Booking Permissions') ?></h4>
        <?php if (!empty($bookingRequest->booking_permissions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Booking Request Id') ?></th>
                <th scope="col"><?= __('Batch Name') ?></th>
                <th scope="col"><?= __('Start Date') ?></th>
                <th scope="col"><?= __('End Date') ?></th>
                <th scope="col"><?= __('Duratiion') ?></th>
                <th scope="col"><?= __('Booking Status') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($bookingRequest->booking_permissions as $bookingPermissions): ?>
            <tr>
                <td><?= h($bookingPermissions->id) ?></td>
                <td><?= h($bookingPermissions->user_id) ?></td>
                <td><?= h($bookingPermissions->booking_request_id) ?></td>
                <td><?= h($bookingPermissions->batch_name) ?></td>
                <td><?= h($bookingPermissions->start_date) ?></td>
                <td><?= h($bookingPermissions->end_date) ?></td>
                <td><?= h($bookingPermissions->duratiion) ?></td>
                <td><?= h($bookingPermissions->booking_status) ?></td>
                <td><?= h($bookingPermissions->created_at) ?></td>
                <td><?= h($bookingPermissions->updated_at) ?></td>
                <td><?= h($bookingPermissions->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'BookingPermissions', 'action' => 'view', $bookingPermissions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'BookingPermissions', 'action' => 'edit', $bookingPermissions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'BookingPermissions', 'action' => 'delete', $bookingPermissions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bookingPermissions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
