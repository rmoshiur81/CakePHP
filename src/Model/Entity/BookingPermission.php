<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BookingPermission Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $booking_request_id
 * @property string $batch_name
 * @property \Cake\I18n\Time $start_date
 * @property \Cake\I18n\Time $end_date
 * @property string $duratiion
 * @property string $booking_status
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $updated_at
 * @property \Cake\I18n\Time $deleted_at
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\BookingRequest $booking_request
 * @property \App\Model\Entity\ClassInHolidayPermission[] $class_in_holiday_permissions
 * @property \App\Model\Entity\ClassScheduleDate[] $class_schedule_dates
 */
class BookingPermission extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
