<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Holiday Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $start_date
 * @property int $total_holiday
 * @property \Cake\I18n\Time $end_date
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $updated_at
 * @property \Cake\I18n\Time $deleted_at
 *
 * @property \App\Model\Entity\ClassInHolidayPermission[] $class_in_holiday_permissions
 * @property \App\Model\Entity\ClassInHolidayRequest[] $class_in_holiday_requests
 */
class Holiday extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
