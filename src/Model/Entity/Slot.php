<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Slot Entity
 *
 * @property int $id
 * @property string $name
 * @property \Cake\I18n\Time $start_time
 * @property \Cake\I18n\Time $end_time
 * @property int $duration
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $updated_at
 * @property \Cake\I18n\Time $deleted_at
 *
 * @property \App\Model\Entity\BookingRequest[] $booking_requests
 * @property \App\Model\Entity\DaySlot[] $day_slot
 */
class Slot extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
