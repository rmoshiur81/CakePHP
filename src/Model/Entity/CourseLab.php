<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CourseLab Entity
 *
 * @property int $course_id
 * @property int $lab_id
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $updated_at
 * @property \Cake\I18n\Time $deleted_at
 *
 * @property \App\Model\Entity\Course $course
 * @property \App\Model\Entity\Lab $lab
 */
class CourseLab extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'course_id' => false,
        'lab_id' => false
    ];
}
