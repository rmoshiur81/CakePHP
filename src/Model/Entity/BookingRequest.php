<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BookingRequest Entity
 *
 * @property int $id
 * @property int $slot_id
 * @property int $user_id
 * @property int $lab_id
 * @property int $course_id
 * @property string $offered_course_name
 * @property \Cake\I18n\Time $expected_start_date
 * @property string $expected_duratiion
 * @property \Cake\I18n\Time $expected_end_date
 * @property string $detail
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $updated_at
 * @property \Cake\I18n\Time $deleted_at
 *
 * @property \App\Model\Entity\Slot $slot
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Lab $lab
 * @property \App\Model\Entity\Course $course
 * @property \App\Model\Entity\BookingPermission[] $booking_permissions
 */
class BookingRequest extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
