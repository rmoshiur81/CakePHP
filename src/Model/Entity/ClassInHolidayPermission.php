<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClassInHolidayPermission Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $holiday_id
 * @property int $booking_permission_id
 * @property string $comment
 * @property string $approve_status
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $updated_at
 * @property \Cake\I18n\Time $deleted_at
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Holiday $holiday
 * @property \App\Model\Entity\BookingPermission $booking_permission
 * @property \App\Model\Entity\ClassScheduleDate[] $class_schedule_dates
 */
class ClassInHolidayPermission extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
