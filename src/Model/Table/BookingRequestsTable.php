<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BookingRequests Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Slots
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Labs
 * @property \Cake\ORM\Association\BelongsTo $Courses
 * @property \Cake\ORM\Association\HasMany $BookingPermissions
 *
 * @method \App\Model\Entity\BookingRequest get($primaryKey, $options = [])
 * @method \App\Model\Entity\BookingRequest newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BookingRequest[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BookingRequest|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BookingRequest patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BookingRequest[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BookingRequest findOrCreate($search, callable $callback = null)
 */
class BookingRequestsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('booking_requests');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Slots', [
            'foreignKey' => 'slot_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Labs', [
            'foreignKey' => 'lab_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Courses', [
            'foreignKey' => 'course_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('BookingPermissions', [
            'foreignKey' => 'booking_request_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('offered_course_name', 'create')
            ->notEmpty('offered_course_name')
            ->add('offered_course_name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->date('expected_start_date')
            ->requirePresence('expected_start_date', 'create')
            ->notEmpty('expected_start_date');

        $validator
            ->requirePresence('expected_duratiion', 'create')
            ->notEmpty('expected_duratiion');

        $validator
            ->date('expected_end_date')
            ->requirePresence('expected_end_date', 'create')
            ->notEmpty('expected_end_date');

        $validator
            ->requirePresence('detail', 'create')
            ->notEmpty('detail');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('updated_at')
            ->allowEmpty('updated_at');

        $validator
            ->dateTime('deleted_at')
            ->allowEmpty('deleted_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['offered_course_name']));
        $rules->add($rules->existsIn(['slot_id'], 'Slots'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['lab_id'], 'Labs'));
        $rules->add($rules->existsIn(['course_id'], 'Courses'));

        return $rules;
    }
}
