<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Slots Model
 *
 * @property \Cake\ORM\Association\HasMany $BookingRequests
 * @property \Cake\ORM\Association\HasMany $DaySlot
 *
 * @method \App\Model\Entity\Slot get($primaryKey, $options = [])
 * @method \App\Model\Entity\Slot newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Slot[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Slot|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Slot patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Slot[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Slot findOrCreate($search, callable $callback = null)
 */
class SlotsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('slots');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->hasMany('BookingRequests', [
            'foreignKey' => 'slot_id'
        ]);
        $this->hasMany('DaySlot', [
            'foreignKey' => 'slot_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->time('start_time')
            ->requirePresence('start_time', 'create')
            ->notEmpty('start_time');

        $validator
            ->time('end_time')
            ->requirePresence('end_time', 'create')
            ->notEmpty('end_time');

        $validator
            ->integer('duration')
            ->requirePresence('duration', 'create')
            ->notEmpty('duration');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('updated_at')
            ->allowEmpty('updated_at');

        $validator
            ->dateTime('deleted_at')
            ->allowEmpty('deleted_at');

        return $validator;
    }
}
