<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DaySlot Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Days
 * @property \Cake\ORM\Association\BelongsTo $Slots
 *
 * @method \App\Model\Entity\DaySlot get($primaryKey, $options = [])
 * @method \App\Model\Entity\DaySlot newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DaySlot[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DaySlot|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DaySlot patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DaySlot[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DaySlot findOrCreate($search, callable $callback = null)
 */
class DaySlotTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('day_slot');
        $this->displayField('day_id');
        $this->primaryKey(['day_id', 'slot_id']);

        $this->belongsTo('Days', [
            'foreignKey' => 'day_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Slots', [
            'foreignKey' => 'slot_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('updated_at')
            ->allowEmpty('updated_at');

        $validator
            ->dateTime('deleted_at')
            ->allowEmpty('deleted_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['day_id'], 'Days'));
        $rules->add($rules->existsIn(['slot_id'], 'Slots'));

        return $rules;
    }
}
