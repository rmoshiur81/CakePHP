<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClassInHolidayPermissions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Holidays
 * @property \Cake\ORM\Association\BelongsTo $BookingPermissions
 * @property \Cake\ORM\Association\HasMany $ClassScheduleDates
 *
 * @method \App\Model\Entity\ClassInHolidayPermission get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClassInHolidayPermission newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClassInHolidayPermission[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClassInHolidayPermission|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClassInHolidayPermission patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClassInHolidayPermission[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClassInHolidayPermission findOrCreate($search, callable $callback = null)
 */
class ClassInHolidayPermissionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('class_in_holiday_permissions');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Holidays', [
            'foreignKey' => 'holiday_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('BookingPermissions', [
            'foreignKey' => 'booking_permission_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ClassScheduleDates', [
            'foreignKey' => 'class_in_holiday_permission_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('comment', 'create')
            ->notEmpty('comment');

        $validator
            ->requirePresence('approve_status', 'create')
            ->notEmpty('approve_status');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('updated_at')
            ->allowEmpty('updated_at');

        $validator
            ->dateTime('deleted_at')
            ->allowEmpty('deleted_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['holiday_id'], 'Holidays'));
        $rules->add($rules->existsIn(['booking_permission_id'], 'BookingPermissions'));

        return $rules;
    }
}
