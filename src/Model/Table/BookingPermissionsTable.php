<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BookingPermissions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $BookingRequests
 * @property \Cake\ORM\Association\HasMany $ClassInHolidayPermissions
 * @property \Cake\ORM\Association\HasMany $ClassScheduleDates
 *
 * @method \App\Model\Entity\BookingPermission get($primaryKey, $options = [])
 * @method \App\Model\Entity\BookingPermission newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BookingPermission[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BookingPermission|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BookingPermission patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BookingPermission[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BookingPermission findOrCreate($search, callable $callback = null)
 */
class BookingPermissionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('booking_permissions');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('BookingRequests', [
            'foreignKey' => 'booking_request_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ClassInHolidayPermissions', [
            'foreignKey' => 'booking_permission_id'
        ]);
        $this->hasMany('ClassScheduleDates', [
            'foreignKey' => 'booking_permission_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('batch_name', 'create')
            ->notEmpty('batch_name');

        $validator
            ->date('start_date')
            ->requirePresence('start_date', 'create')
            ->notEmpty('start_date');

        $validator
            ->date('end_date')
            ->requirePresence('end_date', 'create')
            ->notEmpty('end_date');

        $validator
            ->requirePresence('duratiion', 'create')
            ->notEmpty('duratiion');

        $validator
            ->requirePresence('booking_status', 'create')
            ->notEmpty('booking_status');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('updated_at')
            ->allowEmpty('updated_at');

        $validator
            ->dateTime('deleted_at')
            ->allowEmpty('deleted_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['booking_request_id'], 'BookingRequests'));

        return $rules;
    }
}
