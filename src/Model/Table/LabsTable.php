<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Labs Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Branches
 * @property \Cake\ORM\Association\HasMany $BookingRequests
 * @property \Cake\ORM\Association\HasMany $CourseLab
 *
 * @method \App\Model\Entity\Lab get($primaryKey, $options = [])
 * @method \App\Model\Entity\Lab newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Lab[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Lab|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Lab patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Lab[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Lab findOrCreate($search, callable $callback = null)
 */
class LabsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('labs');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->belongsTo('Branches', [
            'foreignKey' => 'branch_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('BookingRequests', [
            'foreignKey' => 'lab_id'
        ]);
        $this->hasMany('CourseLab', [
            'foreignKey' => 'lab_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('code');

        $validator
            ->integer('total_seat')
            ->requirePresence('total_seat', 'create')
            ->notEmpty('total_seat');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('updated_at')
            ->allowEmpty('updated_at');

        $validator
            ->dateTime('deleted_at')
            ->allowEmpty('deleted_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name']));
        $rules->add($rules->existsIn(['branch_id'], 'Branches'));

        return $rules;
    }
}
