<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClassScheduleDates Model
 *
 * @property \Cake\ORM\Association\BelongsTo $BookingPermissions
 * @property \Cake\ORM\Association\BelongsTo $ClassInHolidayPermissions
 *
 * @method \App\Model\Entity\ClassScheduleDate get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClassScheduleDate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClassScheduleDate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClassScheduleDate|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClassScheduleDate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClassScheduleDate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClassScheduleDate findOrCreate($search, callable $callback = null)
 */
class ClassScheduleDatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('class_schedule_dates');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('BookingPermissions', [
            'foreignKey' => 'booking_permission_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ClassInHolidayPermissions', [
            'foreignKey' => 'class_in_holiday_permission_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('class_no')
            ->requirePresence('class_no', 'create')
            ->notEmpty('class_no')
            ->add('class_no', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->date('class_date')
            ->requirePresence('class_date', 'create')
            ->notEmpty('class_date');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('updated_at')
            ->allowEmpty('updated_at');

        $validator
            ->dateTime('deleted_at')
            ->allowEmpty('deleted_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['class_no']));
        $rules->add($rules->existsIn(['booking_permission_id'], 'BookingPermissions'));
        $rules->add($rules->existsIn(['class_in_holiday_permission_id'], 'ClassInHolidayPermissions'));

        return $rules;
    }
}
