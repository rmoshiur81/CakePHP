<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * BookingPermissions Controller
 *
 * @property \App\Model\Table\BookingPermissionsTable $BookingPermissions
 */
class BookingPermissionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'BookingRequests']
        ];
        $bookingPermissions = $this->paginate($this->BookingPermissions);

        $this->set(compact('bookingPermissions'));
        $this->set('_serialize', ['bookingPermissions']);
    }

    /**
     * View method
     *
     * @param string|null $id Booking Permission id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bookingPermission = $this->BookingPermissions->get($id, [
            'contain' => ['Users', 'BookingRequests', 'ClassInHolidayPermissions', 'ClassScheduleDates']
        ]);

        $this->set('bookingPermission', $bookingPermission);
        $this->set('_serialize', ['bookingPermission']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bookingPermission = $this->BookingPermissions->newEntity();
        if ($this->request->is('post')) {
            $bookingPermission = $this->BookingPermissions->patchEntity($bookingPermission, $this->request->data);
            if ($this->BookingPermissions->save($bookingPermission)) {
                $this->Flash->success(__('The booking permission has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The booking permission could not be saved. Please, try again.'));
            }
        }
        $users = $this->BookingPermissions->Users->find('list', ['limit' => 200]);
        $bookingRequests = $this->BookingPermissions->BookingRequests->find('list', ['limit' => 200]);
        $this->set(compact('bookingPermission', 'users', 'bookingRequests'));
        $this->set('_serialize', ['bookingPermission']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Booking Permission id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bookingPermission = $this->BookingPermissions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bookingPermission = $this->BookingPermissions->patchEntity($bookingPermission, $this->request->data);
            if ($this->BookingPermissions->save($bookingPermission)) {
                $this->Flash->success(__('The booking permission has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The booking permission could not be saved. Please, try again.'));
            }
        }
        $users = $this->BookingPermissions->Users->find('list', ['limit' => 200]);
        $bookingRequests = $this->BookingPermissions->BookingRequests->find('list', ['limit' => 200]);
        $this->set(compact('bookingPermission', 'users', 'bookingRequests'));
        $this->set('_serialize', ['bookingPermission']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Booking Permission id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bookingPermission = $this->BookingPermissions->get($id);
        if ($this->BookingPermissions->delete($bookingPermission)) {
            $this->Flash->success(__('The booking permission has been deleted.'));
        } else {
            $this->Flash->error(__('The booking permission could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
