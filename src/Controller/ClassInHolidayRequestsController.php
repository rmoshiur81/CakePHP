<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ClassInHolidayRequests Controller
 *
 * @property \App\Model\Table\ClassInHolidayRequestsTable $ClassInHolidayRequests
 */
class ClassInHolidayRequestsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Holidays']
        ];
        $classInHolidayRequests = $this->paginate($this->ClassInHolidayRequests);

        $this->set(compact('classInHolidayRequests'));
        $this->set('_serialize', ['classInHolidayRequests']);
    }

    /**
     * View method
     *
     * @param string|null $id Class In Holiday Request id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $classInHolidayRequest = $this->ClassInHolidayRequests->get($id, [
            'contain' => ['Users', 'Holidays']
        ]);

        $this->set('classInHolidayRequest', $classInHolidayRequest);
        $this->set('_serialize', ['classInHolidayRequest']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $classInHolidayRequest = $this->ClassInHolidayRequests->newEntity();
        if ($this->request->is('post')) {
            $classInHolidayRequest = $this->ClassInHolidayRequests->patchEntity($classInHolidayRequest, $this->request->data);
            if ($this->ClassInHolidayRequests->save($classInHolidayRequest)) {
                $this->Flash->success(__('The class in holiday request has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The class in holiday request could not be saved. Please, try again.'));
            }
        }
        $users = $this->ClassInHolidayRequests->Users->find('list', ['limit' => 200]);
        $holidays = $this->ClassInHolidayRequests->Holidays->find('list', ['limit' => 200]);
        $this->set(compact('classInHolidayRequest', 'users', 'holidays'));
        $this->set('_serialize', ['classInHolidayRequest']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Class In Holiday Request id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $classInHolidayRequest = $this->ClassInHolidayRequests->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $classInHolidayRequest = $this->ClassInHolidayRequests->patchEntity($classInHolidayRequest, $this->request->data);
            if ($this->ClassInHolidayRequests->save($classInHolidayRequest)) {
                $this->Flash->success(__('The class in holiday request has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The class in holiday request could not be saved. Please, try again.'));
            }
        }
        $users = $this->ClassInHolidayRequests->Users->find('list', ['limit' => 200]);
        $holidays = $this->ClassInHolidayRequests->Holidays->find('list', ['limit' => 200]);
        $this->set(compact('classInHolidayRequest', 'users', 'holidays'));
        $this->set('_serialize', ['classInHolidayRequest']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Class In Holiday Request id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $classInHolidayRequest = $this->ClassInHolidayRequests->get($id);
        if ($this->ClassInHolidayRequests->delete($classInHolidayRequest)) {
            $this->Flash->success(__('The class in holiday request has been deleted.'));
        } else {
            $this->Flash->error(__('The class in holiday request could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
