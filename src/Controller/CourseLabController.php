<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CourseLab Controller
 *
 * @property \App\Model\Table\CourseLabTable $CourseLab
 */
class CourseLabController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Courses', 'Labs']
        ];
        $courseLab = $this->paginate($this->CourseLab);

        $this->set(compact('courseLab'));
        $this->set('_serialize', ['courseLab']);
    }

    /**
     * View method
     *
     * @param string|null $id Course Lab id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $courseLab = $this->CourseLab->get($id, [
            'contain' => ['Courses', 'Labs']
        ]);

        $this->set('courseLab', $courseLab);
        $this->set('_serialize', ['courseLab']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $courseLab = $this->CourseLab->newEntity();
        if ($this->request->is('post')) {
            $courseLab = $this->CourseLab->patchEntity($courseLab, $this->request->data);
            if ($this->CourseLab->save($courseLab)) {
                $this->Flash->success(__('The course lab has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The course lab could not be saved. Please, try again.'));
            }
        }
        $courses = $this->CourseLab->Courses->find('list', ['limit' => 200]);
        $labs = $this->CourseLab->Labs->find('list', ['limit' => 200]);
        $this->set(compact('courseLab', 'courses', 'labs'));
        $this->set('_serialize', ['courseLab']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Course Lab id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $courseLab = $this->CourseLab->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $courseLab = $this->CourseLab->patchEntity($courseLab, $this->request->data);
            if ($this->CourseLab->save($courseLab)) {
                $this->Flash->success(__('The course lab has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The course lab could not be saved. Please, try again.'));
            }
        }
        $courses = $this->CourseLab->Courses->find('list', ['limit' => 200]);
        $labs = $this->CourseLab->Labs->find('list', ['limit' => 200]);
        $this->set(compact('courseLab', 'courses', 'labs'));
        $this->set('_serialize', ['courseLab']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Course Lab id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $courseLab = $this->CourseLab->get($id);
        if ($this->CourseLab->delete($courseLab)) {
            $this->Flash->success(__('The course lab has been deleted.'));
        } else {
            $this->Flash->error(__('The course lab could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
