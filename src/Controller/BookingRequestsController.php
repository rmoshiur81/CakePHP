<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * BookingRequests Controller
 *
 * @property \App\Model\Table\BookingRequestsTable $BookingRequests
 */
class BookingRequestsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Slots', 'Users', 'Labs', 'Courses']
        ];
        $bookingRequests = $this->paginate($this->BookingRequests);

        $this->set(compact('bookingRequests'));
        $this->set('_serialize', ['bookingRequests']);
    }

    /**
     * View method
     *
     * @param string|null $id Booking Request id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bookingRequest = $this->BookingRequests->get($id, [
            'contain' => ['Slots', 'Users', 'Labs', 'Courses', 'BookingPermissions']
        ]);

        $this->set('bookingRequest', $bookingRequest);
        $this->set('_serialize', ['bookingRequest']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bookingRequest = $this->BookingRequests->newEntity();
        if ($this->request->is('post')) {
            $bookingRequest = $this->BookingRequests->patchEntity($bookingRequest, $this->request->data);
            if ($this->BookingRequests->save($bookingRequest)) {
                $this->Flash->success(__('The booking request has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The booking request could not be saved. Please, try again.'));
            }
        }
        $slots = $this->BookingRequests->Slots->find('list', ['limit' => 200]);
        $users = $this->BookingRequests->Users->find('list', ['limit' => 200]);
        $labs = $this->BookingRequests->Labs->find('list', ['limit' => 200]);
        $courses = $this->BookingRequests->Courses->find('list', ['limit' => 200]);
        $this->set(compact('bookingRequest', 'slots', 'users', 'labs', 'courses'));
        $this->set('_serialize', ['bookingRequest']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Booking Request id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bookingRequest = $this->BookingRequests->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bookingRequest = $this->BookingRequests->patchEntity($bookingRequest, $this->request->data);
            if ($this->BookingRequests->save($bookingRequest)) {
                $this->Flash->success(__('The booking request has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The booking request could not be saved. Please, try again.'));
            }
        }
        $slots = $this->BookingRequests->Slots->find('list', ['limit' => 200]);
        $users = $this->BookingRequests->Users->find('list', ['limit' => 200]);
        $labs = $this->BookingRequests->Labs->find('list', ['limit' => 200]);
        $courses = $this->BookingRequests->Courses->find('list', ['limit' => 200]);
        $this->set(compact('bookingRequest', 'slots', 'users', 'labs', 'courses'));
        $this->set('_serialize', ['bookingRequest']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Booking Request id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bookingRequest = $this->BookingRequests->get($id);
        if ($this->BookingRequests->delete($bookingRequest)) {
            $this->Flash->success(__('The booking request has been deleted.'));
        } else {
            $this->Flash->error(__('The booking request could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
