<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Labs Controller
 *
 * @property \App\Model\Table\LabsTable $Labs
 */
class LabsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Branches']
        ];
        $labs = $this->paginate($this->Labs);

        $this->set(compact('labs'));
        $this->set('_serialize', ['labs']);
    }

    /**
     * View method
     *
     * @param string|null $id Lab id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $lab = $this->Labs->get($id, [
            'contain' => ['Branches', 'BookingRequests', 'CourseLab']
        ]);

        $this->set('lab', $lab);
        $this->set('_serialize', ['lab']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $lab = $this->Labs->newEntity();
        if ($this->request->is('post')) {
            $lab = $this->Labs->patchEntity($lab, $this->request->data);
            if ($this->Labs->save($lab)) {
                $this->Flash->success(__('The lab has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The lab could not be saved. Please, try again.'));
            }
        }
        $branches = $this->Labs->Branches->find('list', ['limit' => 200]);
        $this->set(compact('lab', 'branches'));
        $this->set('_serialize', ['lab']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Lab id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $lab = $this->Labs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $lab = $this->Labs->patchEntity($lab, $this->request->data);
            if ($this->Labs->save($lab)) {
                $this->Flash->success(__('The lab has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The lab could not be saved. Please, try again.'));
            }
        }
        $branches = $this->Labs->Branches->find('list', ['limit' => 200]);
        $this->set(compact('lab', 'branches'));
        $this->set('_serialize', ['lab']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Lab id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $lab = $this->Labs->get($id);
        if ($this->Labs->delete($lab)) {
            $this->Flash->success(__('The lab has been deleted.'));
        } else {
            $this->Flash->error(__('The lab could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
