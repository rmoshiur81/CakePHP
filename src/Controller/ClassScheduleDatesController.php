<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ClassScheduleDates Controller
 *
 * @property \App\Model\Table\ClassScheduleDatesTable $ClassScheduleDates
 */
class ClassScheduleDatesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['BookingPermissions', 'ClassInHolidayPermissions']
        ];
        $classScheduleDates = $this->paginate($this->ClassScheduleDates);

        $this->set(compact('classScheduleDates'));
        $this->set('_serialize', ['classScheduleDates']);
    }

    /**
     * View method
     *
     * @param string|null $id Class Schedule Date id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $classScheduleDate = $this->ClassScheduleDates->get($id, [
            'contain' => ['BookingPermissions', 'ClassInHolidayPermissions']
        ]);

        $this->set('classScheduleDate', $classScheduleDate);
        $this->set('_serialize', ['classScheduleDate']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $classScheduleDate = $this->ClassScheduleDates->newEntity();
        if ($this->request->is('post')) {
            $classScheduleDate = $this->ClassScheduleDates->patchEntity($classScheduleDate, $this->request->data);
            if ($this->ClassScheduleDates->save($classScheduleDate)) {
                $this->Flash->success(__('The class schedule date has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The class schedule date could not be saved. Please, try again.'));
            }
        }
        $bookingPermissions = $this->ClassScheduleDates->BookingPermissions->find('list', ['limit' => 200]);
        $classInHolidayPermissions = $this->ClassScheduleDates->ClassInHolidayPermissions->find('list', ['limit' => 200]);
        $this->set(compact('classScheduleDate', 'bookingPermissions', 'classInHolidayPermissions'));
        $this->set('_serialize', ['classScheduleDate']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Class Schedule Date id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $classScheduleDate = $this->ClassScheduleDates->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $classScheduleDate = $this->ClassScheduleDates->patchEntity($classScheduleDate, $this->request->data);
            if ($this->ClassScheduleDates->save($classScheduleDate)) {
                $this->Flash->success(__('The class schedule date has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The class schedule date could not be saved. Please, try again.'));
            }
        }
        $bookingPermissions = $this->ClassScheduleDates->BookingPermissions->find('list', ['limit' => 200]);
        $classInHolidayPermissions = $this->ClassScheduleDates->ClassInHolidayPermissions->find('list', ['limit' => 200]);
        $this->set(compact('classScheduleDate', 'bookingPermissions', 'classInHolidayPermissions'));
        $this->set('_serialize', ['classScheduleDate']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Class Schedule Date id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $classScheduleDate = $this->ClassScheduleDates->get($id);
        if ($this->ClassScheduleDates->delete($classScheduleDate)) {
            $this->Flash->success(__('The class schedule date has been deleted.'));
        } else {
            $this->Flash->error(__('The class schedule date could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
