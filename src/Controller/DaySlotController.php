<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DaySlot Controller
 *
 * @property \App\Model\Table\DaySlotTable $DaySlot
 */
class DaySlotController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Days', 'Slots']
        ];
        $daySlot = $this->paginate($this->DaySlot);

        $this->set(compact('daySlot'));
        $this->set('_serialize', ['daySlot']);
    }

    /**
     * View method
     *
     * @param string|null $id Day Slot id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $daySlot = $this->DaySlot->get($id, [
            'contain' => ['Days', 'Slots']
        ]);

        $this->set('daySlot', $daySlot);
        $this->set('_serialize', ['daySlot']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $daySlot = $this->DaySlot->newEntity();
        if ($this->request->is('post')) {
            $daySlot = $this->DaySlot->patchEntity($daySlot, $this->request->data);
            if ($this->DaySlot->save($daySlot)) {
                $this->Flash->success(__('The day slot has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The day slot could not be saved. Please, try again.'));
            }
        }
        $days = $this->DaySlot->Days->find('list', ['limit' => 200]);
        $slots = $this->DaySlot->Slots->find('list', ['limit' => 200]);
        $this->set(compact('daySlot', 'days', 'slots'));
        $this->set('_serialize', ['daySlot']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Day Slot id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $daySlot = $this->DaySlot->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $daySlot = $this->DaySlot->patchEntity($daySlot, $this->request->data);
            if ($this->DaySlot->save($daySlot)) {
                $this->Flash->success(__('The day slot has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The day slot could not be saved. Please, try again.'));
            }
        }
        $days = $this->DaySlot->Days->find('list', ['limit' => 200]);
        $slots = $this->DaySlot->Slots->find('list', ['limit' => 200]);
        $this->set(compact('daySlot', 'days', 'slots'));
        $this->set('_serialize', ['daySlot']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Day Slot id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $daySlot = $this->DaySlot->get($id);
        if ($this->DaySlot->delete($daySlot)) {
            $this->Flash->success(__('The day slot has been deleted.'));
        } else {
            $this->Flash->error(__('The day slot could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
