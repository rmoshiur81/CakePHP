<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ClassInHolidayPermissions Controller
 *
 * @property \App\Model\Table\ClassInHolidayPermissionsTable $ClassInHolidayPermissions
 */
class ClassInHolidayPermissionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Holidays', 'BookingPermissions']
        ];
        $classInHolidayPermissions = $this->paginate($this->ClassInHolidayPermissions);

        $this->set(compact('classInHolidayPermissions'));
        $this->set('_serialize', ['classInHolidayPermissions']);
    }

    /**
     * View method
     *
     * @param string|null $id Class In Holiday Permission id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $classInHolidayPermission = $this->ClassInHolidayPermissions->get($id, [
            'contain' => ['Users', 'Holidays', 'BookingPermissions', 'ClassScheduleDates']
        ]);

        $this->set('classInHolidayPermission', $classInHolidayPermission);
        $this->set('_serialize', ['classInHolidayPermission']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $classInHolidayPermission = $this->ClassInHolidayPermissions->newEntity();
        if ($this->request->is('post')) {
            $classInHolidayPermission = $this->ClassInHolidayPermissions->patchEntity($classInHolidayPermission, $this->request->data);
            if ($this->ClassInHolidayPermissions->save($classInHolidayPermission)) {
                $this->Flash->success(__('The class in holiday permission has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The class in holiday permission could not be saved. Please, try again.'));
            }
        }
        $users = $this->ClassInHolidayPermissions->Users->find('list', ['limit' => 200]);
        $holidays = $this->ClassInHolidayPermissions->Holidays->find('list', ['limit' => 200]);
        $bookingPermissions = $this->ClassInHolidayPermissions->BookingPermissions->find('list', ['limit' => 200]);
        $this->set(compact('classInHolidayPermission', 'users', 'holidays', 'bookingPermissions'));
        $this->set('_serialize', ['classInHolidayPermission']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Class In Holiday Permission id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $classInHolidayPermission = $this->ClassInHolidayPermissions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $classInHolidayPermission = $this->ClassInHolidayPermissions->patchEntity($classInHolidayPermission, $this->request->data);
            if ($this->ClassInHolidayPermissions->save($classInHolidayPermission)) {
                $this->Flash->success(__('The class in holiday permission has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The class in holiday permission could not be saved. Please, try again.'));
            }
        }
        $users = $this->ClassInHolidayPermissions->Users->find('list', ['limit' => 200]);
        $holidays = $this->ClassInHolidayPermissions->Holidays->find('list', ['limit' => 200]);
        $bookingPermissions = $this->ClassInHolidayPermissions->BookingPermissions->find('list', ['limit' => 200]);
        $this->set(compact('classInHolidayPermission', 'users', 'holidays', 'bookingPermissions'));
        $this->set('_serialize', ['classInHolidayPermission']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Class In Holiday Permission id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $classInHolidayPermission = $this->ClassInHolidayPermissions->get($id);
        if ($this->ClassInHolidayPermissions->delete($classInHolidayPermission)) {
            $this->Flash->success(__('The class in holiday permission has been deleted.'));
        } else {
            $this->Flash->error(__('The class in holiday permission could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
